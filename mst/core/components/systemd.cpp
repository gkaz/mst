/* systemd.cpp -- SystemD configuration generator.
 *
 * Copyright (C) 2024 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2024 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of MST.
 *
 * MST is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MST is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MST.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>

#include "systemd.h"

#include "../configuration.h"
#include "../types/template.h"
#include "core/template_manager.h"
#include "core/platform.h"

Q_LOGGING_CATEGORY(component_systemd_category, "mst.core.component.systemd")

using namespace systemd;

SystemD::SystemD(Configuration& config) : Component("SystemD", config)
{
    /* Do nothing. */
}

void SystemD::configure()
{
     component_configuration.add(MSTD_SERVICE_FILE,
                                 "/etc/systemd/system/mstd.service",
                                 prepare_service_configuration_template());
}

void SystemD::enable()
{
    Platform::system_enable_service("mstd");
}

void SystemD::disable()
{
    Platform::system_set_default_runlevel("graphical");
    Platform::system_disable_service("mstd");
    Platform::system_disable_service("getty@tty1");
}

void SystemD::start()
{
     Platform::system_start_service("mstd");
}

void SystemD::stop()
{
    Platform::system_stop_service("mstd");
}

Template SystemD::prepare_service_configuration_template()
{
    Template tpl = Template_manager::get_instance()->get_template(MSTD_SERVICE_FILE);
    return tpl.set("mstd-path",
                   QString::fromLocal8Bit(INSTALLATION_PREFIX)
                   + "/bin/mstd");
}

QString SystemD::get_version()
{
    try {
        QVector<QString> result = platform::popen_read(
            QString("systemd"),
            QStringList() << "--version",
            QProcess::StandardOutput);
        return (result.length() > 0) ? result[0] : nullptr;
    }  catch (Platform_exception& e) {
        qCCritical(component_systemd_category).noquote()
            << e.what();
        return nullptr;
    }
}
