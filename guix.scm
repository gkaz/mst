(use-modules (guix licenses)
             (guix gexp)
             (gnu packages)
             (gnu packages admin)
             (gnu packages freedesktop)
             (gnu packages gnome)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages xorg)
             (gnu packages wm)
             (gnu packages python)
             (gnu packages docker)
             (gnu packages xdisorg)
             (gnu packages xorg)
             (gnu packages display-managers)
             (gnu packages bash)
             (gnu packages gl)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages texinfo)
             (gnu packages qt)
             (gnu packages gettext)
             (gnu packages linux)
             (guix utils)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix build-system qt)
             (guix build-system gnu)
             ((guix build utils) #:select (alist-replace))
             (ice-9 match)
             ((srfi srfi-1) #:select (alist-delete)))


(define %source-dir (dirname (current-filename)))


(define mst
  (package
   (name    "mst")
   (version "git")
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? (git-predicate %source-dir)))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #t
      #:imported-modules
      (,@%qt-build-system-modules
       (guix build guile-build-system))
      #:modules
      ((guix build gnu-build-system)
       ((guix build qt-build-system)
        #:prefix qt:)
       (guix build utils)
       ((guix build guile-build-system)
        #:select (target-guile-effective-version)))
      #:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'qm-chmod
                                (lambda _
                                  ;; Make sure 'lrelease' can modify the qm files.
                                  (for-each (lambda (po)
                                              (chmod po #o666))
                                            (find-files "i18n" "\\.qm$"))))
                     (add-after 'unpack 'patch
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (substitute* "mst.pro"
                                               (("\\$\\$LRELEASE")
                                                (string-append (assoc-ref inputs "qttools")
                                                               "/bin/lrelease")))
                                  (substitute* (list "templates/rc.lua.template"
                                                     "templates/rc.lua.4.template")
                                               (("/usr/share/awesome")
                                                (format #f
                                                        "~a/share/awesome"
                                                        (assoc-ref inputs "awesome"))))
                                  (substitute* "templates/mstd.service.template"
                                               (("Restart=on-failure")
                                                (string-append
                                                 "Restart=on-failure\n"
                                                 "Environment=XDG_DATA_DIRS=${XDG_DATA_DIRS}:"
                                                 (string-append (assoc-ref inputs "lightdm")
                                                                "/share/")
                                                 "\n")))))
                     (replace 'configure
                              (lambda* (#:key outputs #:allow-other-keys)
                                (let* ((out (assoc-ref outputs "out"))
                                       (invoke-qmake
                                        (lambda ()
                                          (invoke "qmake"
                                                  (string-append "PREFIX=" out)
                                                  (string-append "BINDIR=" out "/bin")
                                                  (string-append "DATADIR=" out "/share")
                                                  (string-append "PLUGINDIR="
                                                                 out
                                                                 "/lib/qt5/plugins")))))
                                  (invoke-qmake)
                                  (let ((cwd (getcwd)))
                                    (chdir "mst")
                                    (invoke-qmake)
                                    (chdir cwd)
                                    (chdir "mstd")
                                    (invoke-qmake)
                                    (chdir cwd)))))
                     (add-after 'install 'mstd-wrap
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (bin (string-append out "/bin"))
                                         (guile-udev (assoc-ref inputs "guile-udev"))
                                         (libx11 (assoc-ref inputs "libx11"))
                                         (libxfixes (assoc-ref inputs "libxfixes"))
                                         (version (target-guile-effective-version))
                                         (scm (string-append "/share/guile/site/"
                                                             version))
                                         (go (string-append  "/lib/guile/"
                                                             version "/site-ccache")))
                                    (wrap-program (string-append bin "/mstd")
                                                  `("LD_LIBRARY_PATH" prefix
                                                    (,(string-append guile-udev "/lib/")
                                                     ,(string-append libx11 "/lib/")
                                                     ,(string-append libxfixes "/lib/")))
                                                  `("GUILE_LOAD_PATH" prefix
                                                    (,(string-append out scm)
                                                     ,(string-append guile-udev scm)))
                                                  `("GUILE_LOAD_COMPILED_PATH" prefix
                                                    (,(string-append out go)
                                                     ,(string-append guile-udev go)))))))
                     (add-after 'mstd-wrap 'qt-wrap
                                (assoc-ref qt:%standard-phases 'qt-wrap)))))
   (native-inputs
    (list automake
          autoconf
          gnu-make
          bash-minimal
          texinfo
          gettext-minimal
          sed))
   (inputs
    (list accountsservice
          awesome
          docker
          docker-cli
          inetutils
          libnotify
          lightdm
          lightdm-gtk-greeter
          libx11
          libxfixes
          bash-minimal
          guile-udev
          virtualgl
          eudev
          guile-3.0
          qtbase-5
          xorg-server
          xdpyinfo
          xrandr
          xset
          procps
          qttools))
   (home-page "https://gitlab.com/gkaz/mst")
   (synopsis
    "Multi-seat configurator.")
   (description
    "MST (Multi-Seat Toolkit) is a graphical multi-seat configurator and a set of
tools that enables easy configuration of multi-seat setups.")
   (license gpl3)))


mst

;;; guix.scm ends here.
