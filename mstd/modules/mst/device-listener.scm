;;; device-listener.scm -- Listen to and handle device events.

;; Copyright (C) 2021-2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2021-2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains procedures to handle connecting/disconnecting
;; USB devices.


;;; Code:

(define-module (mst device-listener)
  #:use-module (udev udev)
  #:use-module (udev monitor)
  #:use-module (udev device)
  #:use-module (mst system)
  #:use-module (mst core config)
  #:use-module (mst core log)
  #:use-module (mst core seat)
  #:export (device-listener-start))


(define (notify-broadcast config message)
  "Broadcast a notify with a MESSAGE across all seats that are listed
in a CONFIG."
  (for-each (lambda (rec)
              (notify-send (seat-display rec) message))
            config))

(define (error-handler monitor error-message)
  (log-error "ERROR: In ~a: ~a~%" monitor error-message))



(define (device-path device)
  (log-info "device-path: Getting the path for ~a ..." device)
  (let ((path (udev-device-get-devpath device)))
    (log-info "device-path: Getting the path for ~a ... DONE" device)
    path))

(define (device-name device)
  (log-info "device-name: Getting the name for ~a ..." device)
  (let ((name (udev-device-get-property-value device "DEVNAME")))
    (log-info "device-name: Getting the name for ~a ... DONE" device)
    name))

(define (device-action device)
  (log-info "device-action: Getting the action for ~a ..." device)
  (let ((action (udev-device-get-property-value device "ACTION")))
    (log-info "device-action: Getting the action for ~a ... DONE" device)
    action))

(define (device-id-path device)
  (log-info "device-id-path: Getting the ID path for ~a ..." device)
  (let ((id-path (udev-device-get-property-value device "ID_PATH")))
    (log-info "device-id-path: Getting the ID path for ~a ... DONE" device)
    id-path))



(define (udev-monitor-callback device config)
  "This callback is called on block device actions."
  (log-info "udev-monitor-callback: Udev monitor callback triggered")
  (catch #t
    (lambda ()
      (when (string=? (udev-device-get-action device) "add")
        (let* ((devpath        (let ((devpath (device-path device)))
                                 (log-info "udev-monitor-callback: devpath: ~a"
                                           devpath)
                                 devpath))
               (display-number (let ((dn (device-path->display-number config
                                                                      devpath)))
                                 (log-info "udev-monitor-callback: display number: ~a"
                                           dn)
                                 dn))
               (user           (let ((user (display-number->user display-number)))
                                 (log-info "udev-monitor-callback: user: ~a"
                                           user)
                                 user))
               (devname        (let ((devname (device-name device)))
                                 (log-info "udev-monitor-callback: user: ~a"
                                           user)
                                 devname)))
          (when devname
            (if user
                (begin
                  (log-info "udev-monitor-callback: Mounting ~a for seat ~a (user ~a)~%"
                            devpath
                            display-number
                            user)
                  (mount devname user))
                (log-warning "udev-monitor-callback: Could not determine user: ~a"
                             devname))))))
    (lambda (key . args)
      (log-warning "udev-monitor-callback: ~a: ~a" key args))))

(define (udev-input-monitor-callback device config)
  "This callback is called on input device actions."
  (log-info "udev-input-monitor-callback: Udev input monitor callback triggered")
  (catch #t
    (lambda ()
      (let ((devpath  (let ((devpath (device-path device)))
                        (log-info "udev-input-monitor-callback: devpath: ~a"
                                  devpath)
                        devpath))
            (action   (let ((action (device-action device)))
                        (log-info "udev-input-monitor-callback: action: ~a"
                                  action)
                        action))
            (id-path  (let ((id-path (device-id-path device)))
                        (log-info "udev-input-monitor-callback: action: ~a"
                                  action)
                        id-path))
            (devname  (let ((devname (device-name device)))
                        (log-info "udev-input-monitor-callback: devname: ~a"
                                  devname)
                        devname)))
        (cond
         ((string=? action "remove")
          (log-info "udev-input-monitor-callback: Device ~a was removed.~%" devname)
          (log-info "udev-input-monitor-callback: ID_PATH: ~a" id-path)
          (let ((seat (config-seat-lookup-by-device config id-path)))
            (when seat
              (let ((message (format #f
                                     (string-append
                                      "Seat input device ~a was removed.~%"
                                      "Please re-connect the device and"
                                      " reboot the computer.")
                                     (if devname
                                         devname
                                         devpath))))
                (notify-broadcast config message)))))
         ((string=? action "add")
          (log-info "udev-input-monitor-callback: Device ~a was added.~%"
                    (if devname
                        devname
                        devpath))))))
    (lambda (key . args)
      (log-warning "udev-input-monitor-callback: ~a: ~a" key args))))


(define (device-listener-start config)
  "Start udev listener."
  (let* ((udev         (make-udev))
         (udev-monitor (make-udev-monitor udev
                                          #:error-callback error-handler
                                          #:filter '("block" "partition")))
         (udev-input-monitor (make-udev-monitor udev
                                                #:error-callback error-handler
                                                #:filter '("usb" "usb_device"))))
    (log-info "Config: ~a~%" config)

    (let ((callback (lambda (device)
                      (udev-monitor-callback device config))))
      (udev-monitor-set-callback! udev-monitor callback))

    (let ((callback (lambda (device)
                      (udev-input-monitor-callback device config))))
      (udev-monitor-set-callback! udev-input-monitor callback))

    (udev-monitor-start-scanning! udev-monitor)
    (udev-monitor-start-scanning! udev-input-monitor)
    (while #t
           (sleep 1))))

;;; device-listener.scm ends here.
